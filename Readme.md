plapadoo Opencast fork
======================

**This is a fork of https://bitbucket.org/opencast-community/opencast**

It is used by the company plapadoo to publish contributions to the project.

This master branch is intentionally empty. Any interesting work will be in a separate branch https://bitbucket.org/plapadoo/opencast/branches/

https://plapadoo.de
